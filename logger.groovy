package utility

import java.time.LocalDateTime
import static resources.GlobalConstants.INFO
import static resources.GlobalConstants.WARN
import static resources.GlobalConstants.ERROR

/**
 * Class to handle global logging for  
 */
class Logger implements Serializable{

    /** pipeline object */
    private Object script

    /** Jenkins ansi color codes */
    private final String INFO_COLOR = '34'
    private final String WARN_COLOR = '33'
    private final String ERROR_COLOR = '31'

    private Boolean isFileLogger
    private String file

    /**
    * Constructor.
    *
    * @param pipeline object
    * @param to enable file logger [optional]
    */
    public Logger(Object script, Boolean enableFileLogger = false){
        this.script = script
        this.isFileLogger = enableFileLogger
        if(enableFileLogger) {
            String fileName = script.env.LOGGER_FILENAME ? script.env.LOGGER_FILENAME : "build_${script.env.BUILD_ID}.log"
            if(script.env.OS && script.env.OS?.toLowerCase() ==~ /.*windows.*/){
                this.file = script.env.WORKSPACE + "\\" + fileName
            }else {
                this.file = script.env.WORKSPACE + "/" + fileName
            }
        }
    }

    /**
    * Generate log file
    *
    * @param String indicating log message
    */
    private void logFileGenerator(String message){
        try{
            if(script.fileExists(file)) {
                script.writeFile(file: file, text: script.readFile(file)+"\n"+message)
            }else{
                script.writeFile(file: file, text: message)
            }
        }catch(Exception e){
            script.print(sanitizeLogMessage(WARN, WARN_COLOR, "Unable to add entry to a log file : ${e.toString()}"))
        }
        
    }

    /**
    * Print Info log message
    *
    * @param String indicating log message
    */
    public void info(def message) {
        script.print(createLogMessage(INFO, INFO_COLOR, message.toString()))
    }

    /**
    * Print Warning log message
    *
    * @param String indicating log message
    */
    public void warn(def message) {
        script.print(createLogMessage(WARN, WARN_COLOR, message.toString()))
    }

    /**
    * Print Error log message
    *
    * @param String indicating log message
    */
    public void error(def message, Boolean abort=true) {
        if (abort) {
            script.print(createLogMessage(ERROR, ERROR_COLOR, message.toString()))
            script.error(message.toString())
        } else {
            script.print(createLogMessage(ERROR, ERROR_COLOR, message.toString()))
        }
    }
    
}
